import React, {useState, useEffect, useMemo} from 'react';
import './App.css';
import './Style/Cursor.css';
import './Style/Style.css';
import DebugConsole from "./Components/DebugConsole";
import JspcEmu from "./Emulator/JspcEmu";
import {formatBinary, formatHex} from "./Helper/StringHelper";

const source = '/sample.spc';
// const source = '/empty.spc';

function App() {
  const [spcData, setSpcData] = useState(null);
  const [debugOutput, setDebugOutput] = useState([]);
  const emulator = useMemo(() => new JspcEmu(), []);

  const debug = {
    log: (msg) => {
      console.log(msg);
      setDebugOutput((old) => [...old, msg])
    },
  }

  useEffect(() => {
    emulator.setDebugger(debug);

    if (!spcData) {
      fetch(source).then((response) =>
        response.arrayBuffer().then((buffer) => {
          const bytes = new Uint8Array(buffer);
          setSpcData(bytes);
        })
      )
    }
  })

  const cpuRegisters = emulator.getRegisters();

  return (
    <div className="App">
      <header className="App-header">
	  {/*Text*/}
      </header>
      <DebugConsole output={debugOutput}/>
      <button onClick={() => debug.log(`Clicked at ${new Date().toLocaleTimeString()}`)}>Add Output</button>
      <button onClick={() => setDebugOutput([])}>Clear</button>
      <button onClick={() => {setDebugOutput([]); console.clear()}}>Clear (Both)</button>
      <br/>
      <br/>
      <fieldset>
        <legend>Emulator</legend>
        <button onClick={() => emulator.load(spcData)} disabled={spcData === null}>Load</button>
        <button onClick={() => emulator.getMetaData()} disabled={!emulator.isLoaded()}>Parse Meta Data</button>
        <button onClick={() => emulator.tick(false)} disabled={!emulator.isLoaded()}>Tick</button>
        <button onClick={() => emulator.tick(true)} disabled={!emulator.isLoaded()}>Tick (Skip NOP)</button>
        <button onClick={() => emulator.dbgTickUntilUnknown()} disabled={!emulator.isLoaded()}>Tick (Until unknown opcode)</button>

        <br/>

        <h5>CPU Registers</h5>
        <pre>
          A:   {formatHex(cpuRegisters.A)}<br/>
          X:   {formatHex(cpuRegisters.X)}<br/>
          Y:   {formatHex(cpuRegisters.Y)}<br/>
          SP:  {formatHex(cpuRegisters.SP)}<br/>
          PC:  {formatHex(cpuRegisters.PC, 4)}<br/>
          PSW: {formatHex(cpuRegisters.PSW)}     | {formatBinary(cpuRegisters.PSW)}<br/>
        </pre>

        <h5>PSW Flags</h5>
        <pre>
          N:  {cpuRegisters.PSW & 0b10000000}    | Negavite<br/>
          V:  {cpuRegisters.PSW & 0b01000000}    | Overflow<br/>
          P:  {cpuRegisters.PSW & 0b00100000}    | Direct page<br/>
          B:  {cpuRegisters.PSW & 0b00010000}    | Break<br/>
          H:  {cpuRegisters.PSW & 0b00001000}    | Half Carry<br/>
          I:  {cpuRegisters.PSW & 0b00000100}    | Interrupt enabled<br/>
          Z:  {cpuRegisters.PSW & 0b00000010}    | Zero<br/>
          C:  {cpuRegisters.PSW & 0b00000001}    | Carry<br/>
        </pre>

      </fieldset>
    </div>
  );
}

export default App;
