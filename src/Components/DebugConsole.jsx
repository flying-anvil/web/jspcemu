// @flow
import React, {useRef, useLayoutEffect} from 'react';

type Props = {
  output: Array<string>
};

export default function DebugConsole(props: Props) {
  const {output} = props;

  const ref = useRef();

  useLayoutEffect(() => {
    ref.current.scrollTop = ref.current.scrollHeight;
  })

  return (
    <pre className='debug-console' ref={ref}>
      {output.map((line, index) => <React.Fragment key={index}>{line}<br/></React.Fragment>)}
    </pre>
  );
}
