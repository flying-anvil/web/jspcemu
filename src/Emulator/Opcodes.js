// @flow

import type {CpuRegisters} from "./JspcEmu";
import {formatHex, opcodeToString} from "../Helper/StringHelper";
import {opcodeHelper} from "./OpcodeHelper";

let dpOffset;
let dpAddress;

class Opcodes {
  static eval(opcode: number, cpuRegisters: CpuRegisters, ram: Uint8Array, ramDataView: DataView, debug: Object) {
    switch (opcode) {
      case 0x00: // NOP | No Operation
        break;

      // 8-bit Data Transmission (Read)
      // <editor-fold desc="8-bit Data Transmission (Read)">
      case 0xE8: // MOV A, #imm | A <- imm
        cpuRegisters.A = ram[cpuRegisters.PC + 1];
        break;
      case 0xE6: // MOV A, (X) | A <- (X)
        cpuRegisters.A = cpuRegisters.X;
        break;
      case 0xBF: // MOV A, (X)+ | A <- (X), X is incremented afterward
        cpuRegisters.A = cpuRegisters.X++;
        break;
      case 0xE4: // MOV A, dp | A <- (dp)
        dpOffset  = cpuRegisters.PSW & 0b00100000 ? 0x0100 : 0x0000;
        dpAddress = ram[cpuRegisters.PC + 1];
        cpuRegisters.A = ram[dpAddress + dpOffset];
        break;
      case 0xF4: // MOV A, dp | A <- (dp+X)
        dpOffset  = cpuRegisters.PSW & 0b00100000 ? 0x0100 : 0x0000;
        dpAddress = ram[cpuRegisters.PC + 1];
        cpuRegisters.A = ram[dpAddress + dpOffset + cpuRegisters.X];
        break;
      case 0xE5: // MOV A, !abs | A <- (abs)
        cpuRegisters.A = ramDataView.getUint16(cpuRegisters.PC + 1);
        break;
      case 0xF5: // MOV A, !abs+X | A <- (abs+X)
        cpuRegisters.A = ramDataView.getUint16(cpuRegisters.X + cpuRegisters.PC + 1);
        break;
      case 0xF6: // MOV A, !abs+Y | A <- (abs+Y)
        cpuRegisters.A = ramDataView.getUint16(cpuRegisters.Y + cpuRegisters.PC + 1);
        break;
      case 0xE7: // MOV A, [dp+X] | A <- (abs:(abs+X))
        // cpuRegisters.A = ;
        debug.log('Not implemented');
        break;
      case 0xF7: // MOV A, [dp+Y] | A <- (abs:(abs+Y))
        // cpuRegisters.A = ;
        debug.log('Not implemented');
        break;
      case 0xCD: // MOV X, #imm | X <- imm
        cpuRegisters.X = ram[cpuRegisters.PC + 1];
        break;
      case 0xF8: // MOV X, dp | X <- (dp)
        dpOffset  = cpuRegisters.PSW & 0b00100000 ? 0x0100 : 0x0000;
        dpAddress = ram[cpuRegisters.PC + 1];
        cpuRegisters.X = ram[dpAddress + dpOffset];
        break;
      case 0xF9: // MOV X, dp+Y | X <- (dp+Y)
        dpOffset  = cpuRegisters.PSW & 0b00100000 ? 0x0100 : 0x0000;
        dpAddress = ram[cpuRegisters.PC + 1];
        cpuRegisters.X = ram[dpAddress + dpOffset + cpuRegisters.Y];
        break;
      case 0xE9: // MOV X, !abs | X <- (abs)
        cpuRegisters.X = ramDataView.getUint16(cpuRegisters.PC + 1)
        break;
      case 0x8D: // MOV Y, #imm | Y <- imm
        cpuRegisters.Y = ram[cpuRegisters.PC + 1];
        break;
      case 0xEB: // MOV Y, dp | Y <- (dp)
        dpOffset  = cpuRegisters.PSW & 0b00100000 ? 0x0100 : 0x0000;
        dpAddress = ram[cpuRegisters.PC + 1];
        cpuRegisters.Y = ram[dpAddress + dpOffset];
        break;
      case 0xFB: // MOV Y, dp+X | Y <- (dp+X)
        dpOffset  = cpuRegisters.PSW & 0b00100000 ? 0x0100 : 0x0000;
        dpAddress = ram[cpuRegisters.PC + 1];
        cpuRegisters.Y = ram[dpAddress + dpOffset + cpuRegisters.Y];
        break;
      case 0xEC: // MOV Y, !abs | Y <- (abs)
        cpuRegisters.Y = ramDataView.getUint16(cpuRegisters.PC + 1)
        break;
      // </editor-fold>

      // 8-bit Data Transmission (Write)
      // <editor-fold desc="8-bit Data Transmission (Write)">
      case 0xC6: // MOV (X), A) | A -> (X)
        cpuRegisters.X = cpuRegisters.A;
        break;
      case 0xAF: // MOV (X+), A) | A -> (X)+, X is incremented afterward
        cpuRegisters.X = cpuRegisters.A + 1;
        break;

      case 0xD6: // MOV !abs+Y, A | A -> (abs+Y)
        ramDataView.setUint16(ramDataView.getUint16(cpuRegisters.PC + 1 + cpuRegisters.Y), cpuRegisters.A);
        break;
      // </editor-fold>

      // 8-bit Data Transmission (Reg->Reg, Mem->Mem)
      // <editor-fold desc="8-bit Data Transmission (Reg->Reg, Mem->Mem)">
      case 0xBD:
        cpuRegisters.SP = cpuRegisters.X;
        break;
      // </editor-fold>

      // Program Flow Operations
      // <editor-fold desc="Program Flow Operations">
      case 0xFE: // DBNZ Y,rel | --Y and branch in not zero
        // TODO: Not sure if that's correct
        if (--cpuRegisters.Y !== 0) {
          const rel = ramDataView.getInt8(cpuRegisters.PC + 1)
          cpuRegisters.PC += rel;
        }

        break;
      // </editor-fold>

      case 0x3F: // CALL !abs | Subroutine call: push PC to stack and begin execution from abs
        // TODO: use real stack as part of ram
        // TODO: maybe add n to the stack PC to resume there later
        const stack = cpuRegisters.PC;
        const data = ramDataView.getUint16(cpuRegisters.PC + 1, true);
        cpuRegisters.PC = data;
        cpuRegisters.A = data;
        debug.log(`New PC: ${data.toString(16)}`);
        return 0;

      // PSW Operations
      // <editor-fold desc="PSW Operations">
      case 0x60: // CLRC | Clear Carry
        cpuRegisters.PSW &= 0b11111110;
        break;
      case 0x80: // SETC | Set Carry
        cpuRegisters.PSW |= 0b00000001;
        break;
      case 0xED: // NOTC | Invert Carry                    Force to 8-bit
        cpuRegisters.PSW = (cpuRegisters.PSW ^ 0b00000001) & 0b11111111;
        break;
      case 0xE0: // CLRV // Clear V (and H)
        cpuRegisters.PSW &= 0b10110111;
        break;
      case 0x20: // CLRP | Clear DP page
        cpuRegisters.PSW &= 0b11011111;
        break;
      case 0x40: // SETP | Set DP page
        cpuRegisters.PSW |= 0b00100000;
        break;
      case 0xA0: // EI | Enable Interrupts (which are not supported :/ )
        debug.log('Suspicious operation: EI. Interrupts are not suooported');
        cpuRegisters.PSW |= 0b00000010;
        break
      case 0xC0: // DI | Disable Interrupts (which are not supported :/ )
        debug.log('Suspicious operation: DI. Interrupts are not suooported');
        cpuRegisters.PSW &= 0b11111101;
        break
      // </editor-fold>

      default:
        if (!opcodeHelper[opcode]) {
          debug.log(`Error: Unknown opcode "${formatHex(opcode)} (${opcodeToString(opcode)})`);
          throw new Error(`Error: Unknown opcode "${formatHex(opcode)} (${opcodeToString(opcode)})`);
        }

        debug.log(`Error: Unknown opcode "${formatHex(opcode)} (${opcodeToString(opcode)})`);
        throw new Error(`Error: Unknown opcode "${formatHex(opcode)} (${opcodeToString(opcode)})`);
    }

    return opcodeHelper[opcode].bytesCount;
  }
}

export default Opcodes;
