// @flow

export type OpcodeHelperType = {
  name: string,
  operand: string,
  operation: string,
  bytesCount: number,
  cycleCount: number,
};

export type OpcodeHelpersType = {
  [number]: OpcodeHelperType
}

export const opcodeHelper: OpcodeHelpersType = {
  0x00: {
    name: 'NOP',
    operand: '',
    operation: '',
    bytesCount: 1,
    cycleCount: 2,
  },

  // 8-bit Data Transmission (Read)
  // <editor-fold desc="8-bit Data Transmission (Read)">
  0xE8: {
    name: 'MOV',
    operand: 'A, #imm',
    operation: 'A <- imm',
    bytesCount: 2,
    cycleCount: 2,
  },
  0xE6: {
    name: 'MOV',
    operand: 'A, (X)',
    operation: 'A <- (X)',
    bytesCount: 1,
    cycleCount: 3,
  },
  0xBF: {
    name: 'MOV',
    operand: 'A, (X)+',
    operation: 'A <- (X), X is incremented afterward',
    bytesCount: 1,
    cycleCount: 4,
  },
  0xE4: {
    name: 'MOV',
    operand: 'A, dp',
    operation: 'A <- (dp)',
    bytesCount: 2,
    cycleCount: 3,
  },
  0xF4: {
    name: 'MOV',
    operand: 'A, dp+X',
    operation: 'A <- (dp+X)',
    bytesCount: 2,
    cycleCount: 4,
  },
  0xE5: {
    name: 'MOV',
    operand: 'A, !abs',
    operation: 'A <- (abs)',
    bytesCount: 3,
    cycleCount: 4,
  },
  0xF5: {
    name: 'MOV',
    operand: 'A, !abs+X',
    operation: 'A <- (abs+X)',
    bytesCount: 3,
    cycleCount: 5,
  },
  0xF6: {
    name: 'MOV',
    operand: 'A, !abs+Y',
    operation: 'A <- (abs+Y)',
    bytesCount: 3,
    cycleCount: 5,
  },
  0xE7: {
    name: 'MOV',
    operand: 'A, [dp+X]',
    operation: 'A <- (abs:(abs+X))',
    bytesCount: 2,
    cycleCount: 6,
  },
  0xF7: {
    name: 'MOV',
    operand: 'A, [dp+Y]',
    operation: 'A <- (abs:(abs+Y))',
    bytesCount: 2,
    cycleCount: 6,
  },
  0xCD: {
    name: 'MOV',
    operand: 'X, #imm',
    operation: 'X <- imm',
    bytesCount: 2,
    cycleCount: 2,
  },
  0xF8: {
    name: 'MOV',
    operand: 'X, dp',
    operation: 'X <- (dp)',
    bytesCount: 2,
    cycleCount: 3,
  },
  0xF9: {
    name: 'MOV',
    operand: 'X, dp+Y',
    operation: 'X <- (dp+Y)',
    bytesCount: 2,
    cycleCount: 4,
  },
  0xE9: {
    name: 'MOV',
    operand: 'X, !abs',
    operation: 'X <- (abs)',
    bytesCount: 3,
    cycleCount: 4,
  },
  0x8D: {
    name: 'MOV',
    operand: 'Y, #imm',
    operation: 'Y <- imm',
    bytesCount: 2,
    cycleCount: 2,
  },
  0xEB: {
    name: 'MOV',
    operand: 'Y, dp',
    operation: 'Y <- (dp)',
    bytesCount: 2,
    cycleCount: 3,
  },
  0xFB: {
    name: 'MOV',
    operand: 'Y, dp+X',
    operation: 'Y <- (dp+X)',
    bytesCount: 2,
    cycleCount: 4,
  },
  0xEC: {
    name: 'MOV',
    operand: 'Y, !abs',
    operation: 'Y <- (abs)',
    bytesCount: 3,
    cycleCount: 4,
  },
  // </editor-fold>

  // 8-bit Data Transmission (Write)
  // <editor-fold desc="8-bit Data Transmission (Write)">
  0xC6: {
    name: 'MOV',
    operand: '(X), A',
    operation: 'A -> (X)',
    bytesCount: 1,
    cycleCount: 4,
  },
  0xAF: {
    name: 'MOV',
    operand: '(X)+, A',
    operation: 'A -> (X)+, X is incremented afterward',
    bytesCount: 1,
    cycleCount: 4,
  },

  0xD6: {
    name: 'MOV',
    operand: '!abs+Y, A',
    operation: 'A -> (abs+Y)',
    bytesCount: 3,
    cycleCount: 6,
  },
  // </editor-fold>

  // 8-bit Data Transmission (Reg->Reg, Mem->Mem)
  // <editor-fold desc="8-bit Data Transmission (Reg->Reg, Mem->Mem)">
  0xBD: {
    name: 'MOV',
    operand: 'SP, X',
    operation: 'X -> SP',
    bytesCount: 1,
    cycleCount: 2,
  },
  // </editor-fold>

  // Program Flow Operations
  // <editor-fold desc="Program Flow Operations">
  0xFE: {
    name: 'MOV',
    operand: 'Y,rel',
    operation: '--Y and branch in not zero',
    bytesCount: 2,
    cycleCount: '4/6',
  },
  // </editor-fold>

  // Subroutine Operations
  // <editor-fold desc="Subroutine Operations">
  0x3F: {
    name: 'CALL',
    operand: '!abs',
    operation: 'Subroutine call: push PC to stack and begin execution from abs',
    bytesCount: 3,
    cycleCount: 8,
  },
  // </editor-fold>

  // PSW Operations
  // <editor-fold desc="PSW Operations">
  0x60: {
    name: 'CLRC',
    operand: '',
    operation: '',
    bytesCount: 1,
    cycleCount: 2,
  },
  0x80: {
    name: 'SETC',
    operand: '',
    operation: '',
    bytesCount: 1,
    cycleCount: 2,
  },
  0xED: {
    name: 'NOTC',
    operand: '',
    operation: '',
    bytesCount: 1,
    cycleCount: 3,
  },

  0xE0: {
    name: 'CLRV',
    operand: '',
    operation: '',
    bytesCount: 1,
    cycleCount: 2,
  },

  0x20: {
    name: 'CLRP',
    operand: '',
    operation: '',
    bytesCount: 1,
    cycleCount: 2,
  },
  0x40: {
    name: 'SETP',
    operand: '',
    operation: '',
    bytesCount: 1,
    cycleCount: 2,
  },

  0xA0: {
    name: 'EI',
    operand: '',
    operation: '',
    bytesCount: 1,
    cycleCount: 3,
  },
  0xC0: {
    name: 'DI',
    operand: '',
    operation: '',
    bytesCount: 1,
    cycleCount: 3,
  },
  // </editor-fold>
}
