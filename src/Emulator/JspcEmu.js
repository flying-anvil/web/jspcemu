// @flow

/*
 * http://vspcplay.raphnet.net/spc_file_format.txt
 * https://wiki.superfamicom.org/spc700-reference
 * http://emureview.ztnet.com/developerscorner/SoundCPU/spc.htm
 */

import Opcodes from "./Opcodes";
import {formatHex, opcodeOperationToString, opcodeToString} from "../Helper/StringHelper";

export type CpuRegisters = {
  A: number,
  X: number,
  Y: number,
  SP: number,
  PC: number,
  PSW: number,
}

const validSignature = 'SNES-SPC700 Sound File Data v0.30';

class JspcEmu {
  #spc: Uint8Array = null;
  #header: Uint8Array = null;
  #ram: Uint8Array = null;
  #ramDataView: DataView = null;

  #registers = {
    A: 0x00,
    X: 0x00,
    Y: 0x00,
    SP: 0x00,
    PC: 0x0200,
    PSW: 0x00,
  }

  #debug = {log: () => {}};
  #textDecoder = new TextDecoder('ASCII');

  setDebugger(debug: {debug: Function<string>}) {
    this.#debug = debug;
  }

  debug(msg: string) {
    if (this.#debug) {
      this.#debug.log(msg);
    }
  }

  load(bytes: Uint8Array) {
    this.#spc = bytes;
    this.#header = bytes.subarray(0, 0x100);

    this.reset();

    this.debug('===== Loaded spc =================================');
    const signature = this.#textDecoder.decode(this.#header.subarray(0, 33));

    this.debug(`Signature: ${signature} => ${signature === validSignature ? 'valid' : 'invalid'}`);
    this.debug(`Has ID666: ${this.#header[0x23] === 26 ? 'yes' : 'no'}`);
    this.debug(`Version: 0.${this.#header[0x24]}`);
    this.debug('');

    this.debug(`Size: ${bytes.length} Bytes`);
    this.debug(`Header Size: ${this.#header.length} Bytes`);
    this.debug(`RAM Size: ${this.#ram.length} Bytes`);
    this.debug(`Initial PC: ${formatHex(this.#registers.PC, 4)}`);
  }

  isLoaded(): boolean {
    return this.#spc !== null;
  }

  reset() {
    this.#ram = this.#spc.subarray(0x100, 0x10100);

    // Cannot use RAM directly as its buffer is the whole spc
    this.#ramDataView = new DataView(new Uint8Array([...this.#ram]).buffer);
    // this.#registers.PC = this.#dataView.getUint16(0xF4);
    this.#registers = {
      A: this.#header[0x27],
      X: this.#header[0x28],
      Y: this.#header[0x29],
      SP: this.#header[0x2B],
      PC: this.#header[0x25] + (this.#header[0x26] << 8), // Load 2 bytes as Little Endian
      // PC: 0x200, // Seems logical and sample begins by resetting registers, but soon overrides memory, seems suspicious
      PSW: this.#header[0x2A],
    };
  }

  tick(skip: boolean = false, throwError = false) {
    this.debug('===== Tick =======================================');

    let opcode = this.#ram[this.#registers.PC];

    if (skip) {
      let skipped = 0;

      while (opcode === 0x00) {
        this.#registers.PC += 1;
        opcode = this.#ram[this.#registers.PC];

        skipped++
      }

      skipped > 0 && this.debug(`Skipped ${skipped} NOP`);
    }

    this.debug(`PC:        ${formatHex(this.#registers.PC, 4)}`);
    this.debug(`Opcode:    ${formatHex(opcode)} (${opcodeToString(opcode)})`);
    this.debug(`Operation: ${opcodeOperationToString(opcode)}`);

    try {
      // Not inline as that would render PC overwrites in opcodes useless
      const pcOffset = Opcodes.eval(opcode, this.#registers, this.#ram, this.#ramDataView, this.#debug);
      this.#registers.PC += pcOffset;
    } catch (error) {
      // Error is already logged
      if (throwError) {
        throw throwError;
      }

      return;
    }

    this.debug(`New PC: ${formatHex(this.#registers.PC, 4)}`);
  }

  dbgTickUntilUnknown() {
    try {
      for (let i = 0; i < 1000; i++) {
        this.tick(true, true);
      }
    } catch (error) {
    }
  }

  #readString(offset: number, length: number): string {
    return this.#textDecoder.decode(this.#spc.subarray(offset, offset + length)).replace(/\0/g, '');
  }

  getRegisters() {
    return {...this.#registers};
  }

  getMetaData() {
    const songTitle        = this.#readString(0x2E, 32);
    const gameTitle        = this.#readString(0x4E, 32);
    const dumper           = this.#readString(0x6E, 16);
    const comment          = this.#readString(0x7E, 32);
    const originalComposer = this.#readString(0xB1, 32);

    this.debug('===== Meta Data ==================================');
    this.debug(`Song Title: ${songTitle}`);
    this.debug(`Game Title: ${gameTitle}`);
    this.debug(`Dumper: ${dumper}`);
    this.debug(`Comment: ${comment}`);
    this.debug(`Original Composer: ${originalComposer}`);

    return {
      songTitle: songTitle,
      gameTitle: gameTitle,
      dumper: dumper,
      comment: comment,
      originalComposer: originalComposer,
    }
  }
}

export default JspcEmu;
