// @flow

import {opcodeHelper} from "../Emulator/OpcodeHelper";

export function formatHex(input: number, length = 2): string {
  return `0x${input.toString(16).padStart(length, '0').toUpperCase()}`
}

export function formatBinary(input, length = 8): string {
  return input.toString(2).padStart(length, '0')
}

export function opcodeToString(opcode: number): string {
  const {name, operand} = opcodeHelper[opcode] || {
    name: 'no name',
    operand: '',
  }

  if (operand) {
    return `${name} ${operand}`;
  }

  return name;
}


export function opcodeOperationToString(opcode: number): string {
  const {operation} = opcodeHelper[opcode] || {
    operation: 'Unknown',
  }

  return operation;
}
